import sqlalchemy as sq
from sqlalchemy.orm import declarative_base
import sqlalchemy
from sqlalchemy.orm import sessionmaker

from fastapi import  FastAPI

from typing import List
from pydantic import BaseModel, Field

Base = declarative_base()

class Course(Base):
     __tablename__ = "result_4"

     id = sq.Column(sq.Integer, primary_key=True)
     name = sq.Column(sq.String)
     gender = sq.Column(sq.String)

     def __str__(self):
          return f'{self.id},{self.name} ,{self.gender}'


def create_tables():
     DSN = f"postgresql://postgres:lutsenko11@localhost:5432/Test"
     engine = sqlalchemy.create_engine(DSN)
     Base.metadata.create_all(engine)
     # Base.metadata.drop_all(engine)


create_tables()
def session():
    DSN = f"postgresql://postgres:lutsenko11@localhost:5432/Test"
    engine = sqlalchemy.create_engine(DSN)
    Session = sessionmaker(bind=engine)
    session = Session()

    database_line =[]
    for id in session.query(Course).all():
        number = id
        number_1 = str(number)
        data_ingridient = number_1.split(',')
        database_line.append(data_ingridient)
    database = []
    for list in database_line:
        data_user = dict.fromkeys(['id', 'name', 'gender'])
        data_user['id'] = list[0]
        data_user['name'] = list[1]
        data_user['gender'] = list[2]
        database.append(data_user)
    print(database)
    return database

users_1 = session()
print(users_1)

app = FastAPI(title = "Test App")

@app.get("/users/{gender}")

def get_users(gender: str,limit:int, offset:int):
    return [user for user in  users_1[offset:][:limit] if user.get('gender') == gender]

@app.get("/users")

def get_users():
    return users_1

class Users(BaseModel):
    id: str
    name: str
    gender: str

@app.post("/users")
def get_users(users:List[Users]):
    users_1.extend(users)
    return {'status':200,'data' :users_1}

if __name__ == '__main__':
    import uvicorn

    uvicorn.run(app)
